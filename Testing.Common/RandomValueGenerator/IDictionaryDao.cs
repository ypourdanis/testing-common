﻿namespace RandomValueGenerator
{
    /// <summary>
    /// The dictionary data access object.
    /// </summary>
    public interface IDictionaryDao
    {
        /// <summary>
        /// Returns a random word.
        /// </summary>
        ///
        /// <returns>The random word.</returns>
        string GetWord();
    }
}